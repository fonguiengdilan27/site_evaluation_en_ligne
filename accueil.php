<?php
 

include_once 'dbconnection.php';


  if(isset($_POST['envoyer'])){

    if(!empty($_POST['nom']) && !empty($_POST['commentaire']) ){
        $nom =htmlspecialchars($_POST['nom']);
            $com =htmlspecialchars($_POST['commentaire']);

            $req = $bdd->prepare("INSERT INTO commentaire( nom , commentaire) VALUES (? , ?)");
      $req->execute(array($nom,$com));
    }
    $req->closeCursor();          
  }
  
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Project Web || COMPOZ ON LINE </title>
        <link  rel="stylesheet" href="sstyle.css"/>  
        <link  rel="stylesheet" href="bootstrap.min.css"/>
        <link  rel="stylesheet" href="bootstrap-theme.min.css"/>   
        <link rel="stylesheet" href="main.css">
        <script src="jquery.js" type="text/javascript"></script>
        <script src="bootstrap.min.js"  type="text/javascript"></script>
            
</head>
<body>
<header>
        <div id="wrapper">
            <nav>
                <div id="menu1">
                    <a href="#" class="col"><span>COL</span>Compoz On line</a>
                    <a href="inscription.php" class="con" >s'inscrire</a>
                    <a href="requete.php" class="con">feedback</a>
                    <a href="#" class="con  sub1" data-toggle="modal" data-target="#help"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Help</b></span></a></div>
            
                </div>
                <img class="Accueil" src="about.jpg" alt="Accueil">
            </nav>
        </div>
    </header>
    <div class="wrapper2">
        <section>
            <marquee behavior="" direction="" style=" color: white;font-size: 4.5em;letter-spacing: -4px;text-shadow: -1px 2px 1px black;margin:0;"><H1>BIENVENUE SUR COMPOZ ON LINE</H1></marquee>
            <marquee behavior="" direction="right" style=" color: white;font-size: 4.5em;letter-spacing: -4px;text-shadow: -1px 2px 1px black;margin:0;"><H1>SITE WEB DE COMPSITION EN LIGNE</H1></marquee>
        
           </div>
           </section>
                
    <section class="banner">
   
        
        <div class="wrapper3">
        
            <header class="banner_header clearflix" >
                <nav class="banner_nav ">
                    <a href="#" class="col1"><span>COL</span>Compoz On line</a>
                    <ul>
                        <li><a href="requete.php" >Feedback</a></li>
                        <li><a href="inscription.php">inscription</a></li>
                        <li><a href="#about">About us</a></li>
                        <li><a href="#" class="  sub1" data-toggle="modal" data-target="#myModal"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Admin</b></span></a></div></li>
                        <li><a href="#" id="connect" data-toggle="modal" data-target="#etudiant"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>connexion</b></span></a></div></li>
                       
                    </ul> 
                </nav>


            </header>
            <div class="banner_intro">
                <h1>DESCRIPTION ET CARACTERISTIQUE</h1>
               
                <p><br><br>COMPOZ ON LINE est site web d'évaluation en ligne c'est-à-dire un module d'activité permettant à l'enseignant de concevoir et de créer des questionnaires constitués 
                                    d'une grande variété de Type de questions, y compris les questions à choix multiple, les vrai-faux, les réponses courtes et le glisser-déposer d'images et des texte; Ces questions sont conservés dans une banques de questions et peuvent être réutilisées dans différents questionnaires.
                                   <br><p> L'enseignant peut autoriser plusieurs tentatives pour répondre au test et chaque tentative est évaluée automatiquement si le test contient des questions à correction
                                    automatique. Il peut aussi choisir d'afficher les feedback avec ou sans les réponses correctes.</p><br><p> L'enseignant à accés à la liste des étudiants, lire tous les feedback, c'est également lui qui ajoute(crée), modifier ou supprime des quiz. il peut également modifier les informations d'un étudiant ou même de le supprimé. 
                                <b></p><p>Quant à l'étudiant après connection à son espace personnel toutes les matières lui sont proposés mais il dévrait composé que les matières qui lui consernes. Une matière ou UV est caracterisés par son nom ,son code ,son nombre de questions et la cotations(les points pour réponses trouvées et celles érrronées). Une épreuve est un ensemble de QCM; un QCM est un ensemble de 4 questions parmis lesquelles une seule est juste.
                                    L'épreuve est lancé manuellement par l'étudiant, une fois, il a commencé la prémière questions; il est obligés de terminer l'épreuves sinon il obtient une note de 0 pour le reste de questioons non répondu. Ce pendant, sa note et sa mention lui est affichés directement à la fin de l'évaluation d'une épreuve;

                </p>
                <p>me connecter et faire un test</p>
                <a href="inscription.php" class="button_highlight">s'inscrire grauitement</a>
                <div class="banner_slider">
                     <div class="desktop">
                         <div class="toolbar">
                         <Span class="red"></Span>
                         <Span class="orange"></Span>
                         <Span class="green"></Span>
                         </div>
                         <div class="content2">
                             <img src="result.png" alt="dilan" title="dilan" id="image_d">
                         </div>
                     </div>
                    <div class="mobile">
                        <div class="toolbar_m">
                            <span class="micro"></span>
                            <span class="speaker"></span>
                        </div>
                        <div class="content_m">
                            <div class="top_bar">
                                <time class="time" id="time">08:40 PM </time><br>
                                <i class="fa fa-lock"><span class="website" id="website">Dilan.design</span></i>
                            </div>
                            <img src="class.png" alt="background" id="image_m">
                        </div>
                    </div> 

                </div>
            </div>
        </div>
          
    </section>
    <SECTION class="signup clearflix">
        <div class="wrapper3">
            <div class="left clearflix">
                <h3>rejoignez la communauté  de plus de 10 000 étudiants
                    et faites comme eux : Composer rapidement et à distance avec COMPOZ ON LINE et simplifier le coût de votre vie.
                </h3>
                <p>Me connecter en tant que <a href="#" class=" btn sub1" data-toggle="modal" data-target="#myModal"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Admin</b></span></a></div></p>
                    <br><br>
                    <br><br>
            </div>
            <div class="right">
                <a href="inscription.php" class="button_highlight">sign up for free</a>
              </div>
              
              <h3>sites web associés </h3>
        <img src="capture1.png" class="clearflix" alt="siteweb">
        <aside id="commentaire" class="clearflix">
            <div class="rightside clearflix">
                <h1>Commentaires</h1>
                <p>Merci de nous laisser un commentaire sur la page</p><br>
               <form action="" method="post">
                    <fieldset>
                        <legend>commenter</legend>
                        <label for="nom">Nom :</label>
                        <input type="text" name="nom" placeholder="entrer votre nom..."><br><br>
                        <textarea name="commentaire" id="" cols="40" rows="5" placeholder="ecrire un message..."></textarea><br><br>
                        <input type="submit" name="envoyer" value="Envoyer">

                    </fieldset>
               </form>
              
            </div>
           
            <div class="ecran">
                <span> commentaires recents</span>
                <div class="affichage clearflix">
                   
                <?php
                        $allmsg= $bdd->query("SELECT * FROM commentaire ORDER BY id DESC LIMIT 0, 6");
                        
                        while($msg = $allmsg->fetch()){
                            ?>
                            <div class="bordure">
                            <strong><?php echo $msg['nom']; ?> : </strong> <?php echo $msg['commentaire']; ?>
                            </div> 
                            <?php
                        }
                        
                        $allmsg->closeCursor();
                ?>
                </div>
                
            </div>
           
        </aside>
        </div>
       
    </SECTION >
   

   
    <footer>
        <div class="wrapper3">
        <h3>Les Langages suivants sont les outils qui ont permis à la création de ce site web d'évaluation en ligne:</h3>
            <div class="team">
            <ul>
                <li>
                    <img src=" html.png" alt="Team">
                    <p>html</p>
                </li>
                <li>
                    <img src=" css.png" alt="Team">
                    <p>css</p>
                </li>
                <li>
                    <img src=" pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
                <li>
                    <img src=" pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
                <li>
                    <img src=" pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
            </ul>
        </div>
        <hr>
       <br><br><hr>
       <div id="about">
       <h1>A Propos de nous</h1>
        <h3>Nous sommes des jeunes Etudiant en Informatique niveau 2 de l'universités de Dschang. Anthousiaste, intuitif, et determinés, nous vous proposons cette outil pour plus d'interactivité entre
            enseignant et étudiants; Tout en mettant en rigueur les mesures barrières au COVID-19.
        </h3>
            <div class="team clearflix">
            <ul>
                <li>
                    <img src=" bb.jpg" alt="Team">
                    <p>Bryonnaise <br><strong>whatsapp</strong> +237 6 96 75 44 04 </p>
                </li>
                <li>
                    <img src="bd.jpg" alt="Team">
                    <p>Divine <br><strong>whatsapp</strong> +237 6 55 44 47 82 </p>
                </li>
                <li>
                    <img src=" dilan.jpg" alt="Team">
                    <p>Dilan <br><strong>whatsapp</strong> +237 6 99 53 27 23 </p>
                </li>
                <li>
                    <img src=" by.jpg" alt="Team">
                    <p>Marie <br><strong>whatsapp</strong> +237 6 53 58 05 38 </p>
                </li>
                <li>
                    <img src=" bm.jpg" alt="Team">
                    <p>Ivan <br><strong>whatsapp</strong> +237 6 76 76 93 49 </p>
                </li>
            </ul>
        </div>
        </div><br><br>
                            <hr>
        <h3>Gestion des Etudiants</h3>
        <p>COMPOZ ON LINE permet à tout étudiant de pouvoir:</p>
            <div class="team">
            <ul>
                <li>
                    <h4>Se connecter</h4>
                    <p>avoir un espace privée</p>
                </li>
                <li>
                    <h4>Composer</h4>
                    <p>Et cela en ligne pas conséquent à distance</p>
                </li>
                <li>
                   <h4>Faire des consultation</h4>
                    <p>du genre consulter ses notes, sa mentions, son rang, la correction des différentes épreuves</p>
                </li>
                <li>
                    <h4>Faire des Requêtes</h4>
                    <p>c'est un espace communication privé entre enseignants et étudiants</p>
                </li>
                <li>
                    <h4>Apprendre</h4>
                    <p>dans le forum l'étudiant ou le visiteur peut poser différents questions et pourras obtenir la reponse de toute la communauté éducative</p>
                </li>
            </ul>
        </div>
        <p>Se connecter pour plus de fonctionnaliter</p>
        
        <a href="#" class=" btn-info btn-lg sub1" data-toggle="modal" data-target="#myModal"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Je suis enseignant</b></span></a></div><br>
        <a href="#" class="left btn-success btn-lg sub1" data-toggle="modal" data-target="#etudiant"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Se connecter</b></span></a></div>
        </div>
    </footer>
    <script src="accueil.js"></script>
    
    <div class="col-md-2 col-md-offset-4">
            <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content title1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="btn-danger">&times;</span></button>
                    <h4 class="modal-title title1">ESPACE ADMIN : Connexion</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="adminconnect.php" method="POST">
            <fieldset>


            <!-- Text input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="email"></label>  
            <div class="col-md-6">
            <input id="email" name="email" placeholder="Enter votre id-email" class="form-control input-md" type="email">
                
            </div>
            </div>


            <!-- Password input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="password"></label>
            <div class="col-md-6">
                <input id="password" name="password" placeholder="Enter votre Password" class="form-control input-md" type="password">
                
            </div>
            </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                    <button type="submit" class="btn btn-primary">Loger</button>
                    </fieldset>
            </form>
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--sign in modal closed-->

            </div><!--header row closed-->
            </div>


            <div class="col-md-2 col-md-offset-4">
            <div class="modal fade" id="help">
            <div class="modal-dialog">
                <div class="modal-content title1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="btn-danger">&times;</span></button>
                    <h4 class="modal-title title1">Principe de l'évaluation</h4>
                </div>
                <div class="modal-body">
                <p> L'enseignant peut autoriser plusieurs tentatives pour répondre au test et chaque tentative est évaluée automatiquement si le test contient des questions à correction
                                    automatique. Il peut aussi choisir d'afficher les feedback avec ou sans les réponses correctes.</p><br><p> L'enseignant à accés à la liste des étudiants, lire tous les feedback, c'est également lui qui ajoute(crée), modifier ou supprime des quiz. il peut également modifier les informations d'un étudiant ou même de le supprimé. 
                                <b></p><p>Quant à l'étudiant après connection à son espace personnel toutes les matières lui sont proposés mais il dévrait composé que les matières qui lui consernes. Une matière ou UV est caracterisés par son nom ,son code ,son nombre de questions et la cotations(les points pour réponses trouvées et celles érrronées). Une épreuve est un ensemble de QCM; un QCM est un ensemble de 4 questions parmis lesquelles une seule est juste.
                                    L'épreuve est lancé manuellement par l'étudiant, une fois, il a commencé la prémière questions; il est obligés de terminer l'épreuves sinon il obtient une note de 0 pour le reste de questioons non répondu. Ce pendant, sa note et sa mention lui est affichés directement à la fin de l'évaluation d'une épreuve;

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">OKAY</button>
                   
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--sign in modal closed-->

            </div><!--header row closed-->
            </div>


            <div class="col-md-2 col-md-offset-4">
            <div class="modal fade" id="etudiant">
            <div class="modal-dialog">
                <div class="modal-content title1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="btn-danger">&times;</span></button>
                    <h4 class="modal-title title1">ESPACE ETUDIANT : Connexion</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="login.php" method="POST">
            <fieldset>


            <!-- Text input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="email"></label>  
            <div class="col-md-6">
            <input id="email" name="email" placeholder="Entrer votre id-email" class="form-control input-md" type="email">
                
            </div>
            </div>


            <!-- Password input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="password"></label>
            <div class="col-md-6">
                <input id="password" name="password" placeholder="Entrer votre Password" class="form-control input-md" type="password">
                
            </div>
            </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                    <button type="submit" class="btn btn-primary">Se connecter</button>
                    </fieldset>
            </form>
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--sign in modal closed-->

            </div><!--header row closed-->
            </div>

            <div class="col-md-2 col-md-offset-4">
            <div class="modal fade" id="etud_connect">
            <div class="modal-dialog">
                <div class="modal-content title1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="btn-danger">&times;</span></button>
                    <h4 class="modal-title title1">ESPACE ETUDIANT Connexion</h4>
                </div>
                <div class="modal-body">
                  
                <?php if(@$_GET['message'])
                                {echo'<script>alert("'.@$_GET['message'].'");</script>';}
                        ?>

          
                </div>
                <div class="modal-footer">
                <button type="submit" class="btn btn-primary">OKAY</button>
                
        
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--sign in modal closed-->

            </div><!--header row closed-->
            </div>

</body>
</html>