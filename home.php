<!DOCTYPE html>
<?php include_once 'dbConnection.php';
session_start();
$email=$_SESSION['email'];
  if(!(isset($_SESSION['email']))){
header("location:accueil.php?message=bien vouloir se connecter");

}
else
{
$name = $_SESSION['name'];

include_once 'dbConnection.php';
}
?>
<html lang="fr">
<head>
<meta charset="utf-8">
    <title>Projet web || COMPOZ_ON_LINE</title>
    <link rel="stylesheet" type="text/css" href="menu.css">
    <link rel="stylesheet" type="text/css" href="sstyle.css">
    
 <link  rel="stylesheet" href="css/bootstrap-theme.min.css"/>    
 <script src="js/jquery.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js"  type="text/javascript"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">

  </head>
<body>
    <header>
        <div id="wrapper">
            <nav>
                <div id="menu1">
                    <a href="#" class="col"><span>COL</span>Compoz On line</a>
                    <a href="logout.php" class="con">| déconnexion</a>
                    <?php if(isset($name)){ echo '<a href="mes_infos.php" class="con"><b>Hello, </b>' .$name. '</a>';} ?>

                </div>
               
            </nav>
        </div>
    </header>
    <section>
       
   
    <nav >
        <ul id="menu">
          <li><a href="#" class="active">Menu</a></li>
          <li><a href="home.php" >Home</a></li>
          <li><a href="historik.php">Historique</a></li>
          <li><a href="classik.php">Classification</a></li>
          <li><a href="requete.php">Requete</a></li>
          <li> <a href="mes_infos.php">Mes informations</a></li> 
          <li><a href="logout.php">Déconnexion</a></li>


        </ul>
      </nav>
  

  <div class="content">
    
        
          <?php
          $qry = "SELECT * FROM matiere ORDER BY code desc";
            $result = $bdd->query($qry);
            echo  '<div class="panel"><div class="table-responsive"><table class="table table-striped title1">
            <caption>MATIERES DEJA PROGRAMMES</caption>
                    <tr>
                        <th><b>Code</b></th>
                        <th><b>Sujet</b></th>
                        <th><b>Total question</b></th>
                        <th><b>Noté sur</b></th>
                        <th></th>
                    </tr>';

            while($row = $result->fetch()) {
              $title = $row['nom'];
              $total = $row['total_question'];
              $sahi = $row['juste'];
              $eid = $row['code'];
            $q12=$bdd->prepare("SELECT note FROM historique WHERE eid=? AND email=?" );
            $q12->execute(array($eid , $email));
            $rowcount=$q12->rowCount();	
            if($rowcount == 0){
              echo '<tr>
              <td>'.$eid.'</td>
              <td>'.$title.'</td>
              <td>'.$total.'</td>
              <td>'.$sahi*$total.'</td>
              <td><b><a href="compo.php?q=quiz&step=2&eid='.$eid.'&n=1&t='.$total.'" class="pull-right btn sub1" style="margin:0px;background:green;padding:10px;text-decoration:none;"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>>>Start</b></span></a></b></td>
              </tr>';
            }
            else
            {
            echo '<tr style="color:#99cc32"><td>'.$eid.'</td>
            <td>'.$title.'&nbsp;<span title="This quiz is already solve by you" class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>
            <td>'.$total.'</td>
            <td>'.$sahi*$total.'</td>
            <td><b><a href="compo.php?q=quizre&step=25&eid='.$eid.'&n=1&t='.$total.'" class="pull-right btn sub1" style="margin:0px;background:red;padding:10px; text-decoration:none;"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Restart</b></span></a></b></td>
            </tr>';
            }
          }
            echo '</table></div></div>';
      
          ?>
  
  </div>
  <?php if(@$_GET['message'])
                                {echo'<script>alert("'.@$_GET['message'].'");</script>';}
                        ?>

    </section>
   
</body>
</html>