


(function() {
    var dil = {
        run: function(){
            this.initialiZe();
            this.startSlider();
        },
        initialiZe: function(){
            
            
            var desktopImage = document.querySelector('#image_d');
            var mobileImage = document.querySelector('#image_m');
            document.querySelector('#time').innerHTML = this.getCurrentTime();
            var website = document.querySelector('#website');
        },
        startSlider: function(){
            setInterval(function(){
                this.destopImage.src = this.images[this.index]['desktop'];
                this.mobileImage.src = this.images[this.index]['mobile'];
                this.website.innerHTML = this.images[this.index]['website'];
                this.updateIndex();
            }.bind(this), 1000);
        },
        updateIndex: function() {
            
            var taille = images.length - 1;
            if(index < taille){
                index++;
            }else{
                index = 0; 
            }
            return this.index;
        },

        getCurrentTime: function(){
            var date= new Date();
            var hour = date.getHours();
            var minutes = date.getMinutes();
            var amPm = hour>= 12 ? 'PM': 'AM';
            hour = hour % 12;
            hour = hour ? hour :12;
            hour = hour < 10 ? '0'+ hour : hour;
            minutes = minutes < 10 ? '0'+ minutes : minutes;
            return hour + ':' + minutes  + ' ' + amPm;
            
        },
       
        images: [
            {
                desktop: 'dilan.jpg',
                mobile:  'bg.jpg',
                website: 'dilan.design'
            },
            {
                desktop: 'result.png',
                mobile:  'class.png',
                website: 'mickael.design'
            },
            {
                desktop: 'about.jpg',
                mobile:  'dilan.jpg',
                website: 'dilan.design'
            }
        ],
    };


dil.run();
})();