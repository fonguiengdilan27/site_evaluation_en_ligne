<!DOCTYPE html>
<?php
    include_once 'dbconnection.php';

    if(isset($_POST['forminscription'])){

        $nom = htmlspecialchars($_POST['nom']);
        $matricule = htmlspecialchars($_POST['matricule']);
        $filiere = htmlspecialchars($_POST['filiere']);
        $niveau = htmlentities($_POST['niveau']);
        $sexe= htmlspecialchars($_POST['sexe']);
        $email= htmlspecialchars($_POST['email']);
        $numero = htmlentities($_POST['numero']);
        $password = sha1($_POST['password']);
        $confirm = sha1($_POST['confirm']);
    
            $nomlength = strlen($nom);
            if( $nomlength <= 255){

                $reqmat = $bdd->prepare("SELECT * FROM etudiant WHERE matricule= ?");
                $reqmat->execute(array($matricule));
                $matexist = $reqmat->rowCount();
                if($matexist==0){
                    if($niveau>=1 && $niveau<=7){
                        if( filter_var($email, FILTER_VALIDATE_EMAIL)){
    
                            $reqmail = $bdd->prepare("SELECT * FROM etudiant WHERE email= ?");
                            $reqmail->execute(array($email));
                            $mailexist = $reqmail->rowCount();
                            if($mailexist==0){
                                if($numero>=650000000){
                                    $reqcontact = $bdd->prepare("SELECT * FROM etudiant WHERE contact= ?");
                                    $reqcontact->execute(array($numero));
                                    $contactexist = $reqcontact->rowCount();
                                    if($contactexist==0){
                                        $passwordl = strlen($_POST['password']);
                                        if ($passwordl>=4 && $passwordl<=25) {
                                            if ($password == $confirm) {
                                                try{
                                                    $inscritetudiant =$bdd->prepare("INSERT INTO etudiant(nom, matricule,filiere,niveau,sexe,email,contact,passeword) VALUES (:nom, :matricule,:filiere,:niveau,:sexe,:email,:contact,:passeword)");
                                                    $inscritetudiant->execute(
                                                        array(
                                                            'nom'=> $nom,
                                                            'matricule' => $matricule,
                                                            'filiere' => $filiere,
                                                            'niveau' => $niveau,
                                                            'sexe' => $sexe,
                                                            'email' => $email,
                                                            'contact' => $numero,
                                                            'passeword' => $password 
                                                        )
            
                                                    );
                                                    session_start();
                                                    $_SESSION["email"] = $email;
                                                    $_SESSION["name"] = $nom;

                                                    header("location:home.php?message=votre compte a ete bien crée");
                                                   
            
                                                }catch(Exception $e){
                                                    
                                                    die('Erreur :' .$e->getMessage());
                                                }
                                               
                                            }
                                            else{
                                                $erreur = "le mot de passe est incorrect";
                                            }
                                        }
                                        else{
                                            $erreur = "le mot de passe est doit etre compris de [4-25] caractères ";
                                        }
                                          
                                    }
                                    else{
                                        $erreur = "le contact existe déjà";
                                    }        
                                }
                                else{
                                    $erreur = "la forme du conctact est incorrect";
                                }
                           
     
                            }
                            else{
                                $erreur = "le mail existe déjà";
                            }
                           
                                
                        }
                        else{
                            $erreur = "email invalide";
                        }
                    }
                    else{
                        $erreur = "le niveau est compris entre [1-7]";
                    }
                    
                    
                }
                else{
                    $erreur = "le matricule existe déjà";
                }
                
            }else{
                $erreur = "le nom est tres long";
            }
        
    }
        
?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Project Web || COMPOZ ON LINE</title>
    <link rel="stylesheet" href="sstyle.css">
</head>
<body>
    <header>
        <div id="wrapper">
            <nav>
                <div id="menu1">
                    <a href="#" class="col"><span>COL</span>Compoz On line</a>
                    <a href="accueil.php" class="con">Accueil</a>
                </div>
                <img class="Accueil" src="bg.jpg " alt="Accueil">
            </nav>
        </div>
    </header>
    <div class="wrapper2">
        <section>
            <form action="" method="POST">
                <div class="inscription"> 
                    <h1>inscription</h1> 
                    <input type="text" name="nom" required="required" class="box" placeholder="Entrer votre Nom..." value="<?php if(isset($nom)){ echo $nom;} ?>"><br>
                    <input type="text" name ="matricule" class="box" required="required" placeholder="Entrer votre Matricule..." value="<?php if(isset($matricule)){ echo $matricule;} ?>"> <br>
                    <input type="text" name= "filiere" class="box" required="required" placeholder="Entrer votre filiere ..." value="<?php if(isset($filiere)){ echo $filiere;} ?>"> <br>
                    <input type="number" name ="niveau" class="box" required="required" placeholder="Entrer votre niveau ..." value="<?php if(isset($niveau)){ echo $niveau;} ?>"> <br>
                    <label for="sexe" id="sexe ">Que est votre sexe?</label>
                    <select name="sexe" id="sexe1" value="<?php if(isset($sexe)){ echo $sexe;}?>">
                         
                        <option value="Votre nature" disabled="disabled">votre nature</option>
                        <option value="Homme">Masculin</option>
                        <option value="Femme">Feminin</option>
                         
                    </select><br>
                    <input  type="email" name="email" class="box" required="required" placeholder="Entrer votre Email..." value="<?php if(isset($email)){ echo $email;}?>" ><br>
                    <input  type="number" name="numero" class="box" required="required" placeholder="Entrer votre contact, ex:677777777" value="<?php if(isset($numero)){ echo $numero;}?>"> <br>
                    <input type="password" name="password" class="box" required="required" placeholder="Entrer votre password..."> <br>
                    <input type="password" name="confirm" class="box" required="required" placeholder="Confirmer votre password..."> <br>
                    <?php
                if(isset($erreur)){
                    echo '<font color="red">'.$erreur.'</font>';
                }
                ?><br><br> 
                     
                    <input type="submit" name="forminscription" id="registed" value="m'inscrire">
                </div>
            </form>

        </section> 
    </div>  <?php if(@$_GET['message'])
                                {echo'<script>alert("'.@$_GET['message'].'");</script>';}
                        ?>

        
    <marquee style ="border: 3px solid #012;"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150">  <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg"  style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> <img src="Dilan.jpg" style ="border: 3px solid yellow;" width="250" height="150"> </marquee>
    <footer>
        <div class="team">
             <ul>
                 <li>
                     <img src="pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
                <li>
                    <img src="pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
                <li>
                    <img src="dilan.jpg" alt="Team">
                    <p>DILAN</p>
                </li>
                <li>
                    <img src="pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
                <li>
                    <img src ="pic.png" alt="Team">
                    <p>WestLife</p>
                </li>
            </ul>
        </div>
    </footer>
    
</body>
</html>