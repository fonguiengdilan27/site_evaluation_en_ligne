-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 16 juin 2021 à 13:49
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `compoz_on_line`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `password`) VALUES
(1, 'dilan@gmail.com', '123456'),
(2, 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `classification`
--

CREATE TABLE `classification` (
  `email` varchar(50) NOT NULL,
  `note` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `classification`
--

INSERT INTO `classification` (`email`, `note`, `time`) VALUES
('dil@gmail.com', 89, '2021-06-16 10:27:55');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `commentaire` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `nom`, `commentaire`) VALUES
(1, 'dilan', 'nice! ce site'),
(2, 'dilan', 'ce site est trop nice !'),
(3, 'dilan', 'ce site est trop nice !'),
(4, 'dilan', 'ce site est trop nice !'),
(5, 'dilan', 'ce site est trop nice !'),
(6, 'dilan', 'ce site est trop nice !'),
(7, 'dilan', 'ce site est trop nice !'),
(8, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(9, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(10, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(11, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(12, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(13, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(14, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(15, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(16, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(17, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(18, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(19, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(20, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(21, 'RODIA', 'C\'est ce genre d\'ouyil qui nous faut au Cameroun \r\n\r\nj\'aime bien\r\nje laisse mon  pousse bleu :) '),
(22, 'dilan55', 'hi'),
(23, 'dilan55', 'hi'),
(24, 'dilan55', 'hi'),
(25, 'dilan55', 'hi'),
(26, 'dilan55', 'hi'),
(27, 'dilan55', 'hi'),
(28, 'dilan55', 'hi'),
(29, 'dilan55', 'hi'),
(30, 'dilan55', 'hi'),
(31, 'dilan55', 'hi'),
(32, 'dilan55', 'hi'),
(33, 'megane', 'wouahhhh'),
(34, 'ivan', 'bravo!!!!'),
(35, 'dilan', 'boujour la communauté'),
(36, 'dilan', 'boujour la communauté'),
(37, 'dilan', 'boujour la communauté'),
(38, 'marie', 'j\'aime bien'),
(39, 'gires', 'j\'aimes');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `ide` int(255) UNSIGNED NOT NULL,
  `nom` varchar(255) NOT NULL,
  `matricule` varchar(255) NOT NULL,
  `filiere` varchar(255) NOT NULL,
  `niveau` int(2) UNSIGNED ZEROFILL NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` int(11) UNSIGNED NOT NULL,
  `passeword` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`ide`, `nom`, `matricule`, `filiere`, `niveau`, `sexe`, `email`, `contact`, `passeword`) VALUES
(3, 'dilan', 'ddg112251', 'informatique', 02, 'Homme', 'dil@gmail.com', 698654632, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(6, 'joel', 'm003', 'math', 03, 'Homme', 'joel@gmail.com', 677777777, '229be39e04f960e46d8a64cadc8b4534e6bfc364'),
(8, 'marie1', 'm005', 'informatique', 02, 'Femme', 'marie1@gmail.com', 677777768, '229be39e04f960e46d8a64cadc8b4534e6bfc364');

-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

CREATE TABLE `historique` (
  `email` varchar(50) NOT NULL,
  `eid` text NOT NULL,
  `note` int(11) NOT NULL,
  `niveau` int(11) NOT NULL,
  `juste` int(11) NOT NULL,
  `fausse` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `historique`
--

INSERT INTO `historique` (`email`, `eid`, `note`, `niveau`, `juste`, `fausse`, `date`) VALUES
('sunnygkp10@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 09:31:26'),
('sunnygkp10@gmail.com', '558920ff906b8', 4, 2, 2, 0, '2015-06-23 13:32:09'),
('avantika420@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 14:33:04'),
('avantika420@gmail.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 14:49:39'),
('sunnygkp10@gmail.com', '5589741f9ed52', 4, 5, 3, 2, '2015-06-23 15:07:16'),
('mi5@hollywood.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 15:12:56'),
('nik1@gmail.com', '558921841f1ec', 1, 2, 1, 1, '2015-06-23 16:11:50'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'INF244', 1, 1, 1, 0, '2021-06-05 22:05:33'),
('dil@gmail.com', 'jji', 2, 1, 1, 0, '2021-06-05 22:10:06'),
('dil@gmail.com', 'jji', 2, 1, 1, 0, '2021-06-05 22:10:06'),
('dil@gmail.com', 'jji', 2, 1, 1, 0, '2021-06-05 22:10:06'),
('dil@gmail.com', 'jji', 2, 1, 1, 0, '2021-06-05 22:10:06'),
('dil@gmail.com', 'jhj', 1, 1, 1, 0, '2021-06-05 22:10:19'),
('dil@gmail.com', 'loi', 1, 1, 1, 0, '2021-06-05 22:10:24'),
('dil@gmail.com', 'ol2', 1, 1, 1, 0, '2021-06-05 22:10:28'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', '111', 1, 1, 0, 1, '2021-06-06 09:54:16'),
('dil@gmail.com', 'inf111', 2, 1, 0, 1, '2021-06-07 08:01:52'),
('dil@gmail.com', 'inf111', 2, 1, 0, 1, '2021-06-07 08:01:52'),
('dil@gmail.com', 'INF232', 10, 10, 0, 10, '2021-06-15 20:07:42'),
('dil@gmail.com', 'INF232', 10, 10, 0, 10, '2021-06-15 20:07:42'),
('dil@gmail.com', 'INF232', 10, 10, 0, 10, '2021-06-15 20:07:42'),
('dil@gmail.com', 'INF242', 10, 5, 0, 5, '2021-06-16 10:27:55');

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(100) NOT NULL,
  `code` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `total_question` int(10) NOT NULL,
  `juste` int(10) NOT NULL,
  `fausse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `code`, `nom`, `total_question`, `juste`, `fausse`) VALUES
(20, 'INF222', 'Génie Logiciel', 5, 4, -2),
(21, 'INF221', 'Systeme  D\' information', 5, 4, -2),
(22, 'INF262', 'Théorie Des Marchés', 8, 2, -1),
(23, 'INF252', 'Electronique Numerique', 10, 2, -1),
(24, 'INF242', 'Algèbre Linéaire', 5, 4, -2),
(25, 'INF232', 'Programmation web dynamique', 10, 2, -1);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `SN` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `commentaire` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`SN`, `nom`, `email`, `sujet`, `commentaire`) VALUES
(1, 'ivan', 'ivan@gmail.com', 'java', 'formidable'),
(2, 'ivan', 'admin@admin.com', 'css', 'salut'),
(3, 'dilane', 'dilan@gmail.com', 'Reseau', 'c\'est rien je maitrise'),
(4, 'dilane', 'dil@gmail.com', 'SE', 'bonjour j\'aime bien ce site c\'est tres genial , adorable , magnifique'),
(5, 'zidane', 'ndiaivan@gmail.com', 'css', 'ygtrderthb t\'rftb trft -gtgt tgr tf tfrun fftg gygyggfrgygfygrr ftg fytnf r(gtr(_g ((t(r rtgyf fehbfcdgnjltf fttgff fddgff frdrff rftfgd fghrd dfgdhj (ftf tfrrrf ftff tftf tutr rtft rrtr ruttrff rtfftf  rgrfggjgggfj ftgtfj fjggrj ft jfjt ftftft tfdrd ddrdrjfjfjdjydrdjj djj jdjdd');

-- --------------------------------------------------------

--
-- Structure de la table `option`
--

CREATE TABLE `option` (
  `idquestion` text NOT NULL,
  `options` text NOT NULL,
  `idoption` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `option`
--

INSERT INTO `option` (`idquestion`, `options`, `idoption`) VALUES 
('60c9b05071639', 'Le modèle en V', '60c9b0507990f'),
('60c9b05071639', 'Le modèle en cascade', '60c9b05079926'),
('60c9b05071639', 'Le modèle en spiral', '60c9b0507992e'),
('60c9b05071639', 'Le modèle en T', '60c9b05079935'),
('60c9b0508376a', 'La validation', '60c9b050849e5'),
('60c9b0508376a', 'La conception', '60c9b050849f8'),
('60c9b0508376a', 'Le processus', '60c9b05084a00'),
('60c9b0508376a', 'L\'integration', '60c9b05084a07'),
('60c9b0508aad9', 'Diagramme d\'activités', '60c9b0508c3e8'),
('60c9b0508aad9', 'Diagramme de composants', '60c9b0508c3fe'),
('60c9b0508aad9', 'Diagramme de structure composite', '60c9b0508c406'),
('60c9b0508aad9', 'Diagramme de communication', '60c9b0508c40d'),
('60c9b05095eff', 'unified Modeling Language', '60c9b05096f0c'),
('60c9b05095eff', 'unified Modeling Langage', '60c9b05096f1e'),
('60c9b05095eff', 'unifield Modeling Language', '60c9b05096f23'),
('60c9b05095eff', 'rien de tout ceci', '60c9b05096f28'),
('60c9b0509bf9c', 'La portabilité', '60c9b0509ecc7'),
('60c9b0509bf9c', 'La securité', '60c9b0509ecd8'),
('60c9b0509bf9c', 'La robustesse', '60c9b0509ecdd'),
('60c9b0509bf9c', 'La validation', '60c9b0509ece2'),
('60c9b6adca281', 'Système Informatique', '60c9b6adcc272'),
('60c9b6adca281', 'Système D\'Information', '60c9b6adcc28a'),
('60c9b6adca281', 'Synthése D\'information', '60c9b6adcc291'),
('60c9b6adca281', 'rien de ceci', '60c9b6adcc298'),
('60c9b6add4b26', 'Savoir, garder , communiquer', '60c9b6add5fd5'),
('60c9b6add4b26', 'Collecter, Stocker ,Traiter , Diffuser', '60c9b6add5fec'),
('60c9b6add4b26', 'Ordonner les ressource de l\'entreprise', '60c9b6add5ff4'),
('60c9b6add4b26', 'Rien de tout ceci ', '60c9b6add5ffb'),
('60c9b6addbe69', 'Le diagramme de flux', '60c9b6addd20f'),
('60c9b6addbe69', 'Le language SQL', '60c9b6addd226'),
('60c9b6addbe69', 'Le diagramme de Sequence', '60c9b6addd22d'),
('60c9b6addbe69', 'Le diagramme relationnel', '60c9b6addd234'),
('60c9b6aded611', 'Externe', '60c9b6adf007e'),
('60c9b6aded611', 'Fixe', '60c9b6adf0090'),
('60c9b6aded611', 'Actif', '60c9b6adf0096'),
('60c9b6aded611', 'Inactif', '60c9b6adf009a'),
('60c9b6ae00b97', 'Un acteur à un autre acteur', '60c9b6ae01b4f'),
('60c9b6ae00b97', 'Une information à un ordinateur', '60c9b6ae01b61'),
('60c9b6ae00b97', 'Une information à un acteur', '60c9b6ae01b66'),
('60c9b6ae00b97', 'je ne sais pas', '60c9b6ae01b6b'),
('60c9bce929127', 'L\'Etat', '60c9bce92addf'),
('60c9bce929127', 'Entreprises', '60c9bce92adf8'),
('60c9bce929127', 'Les ménages', '60c9bce92ae00'),
('60c9bce929127', 'Un téléphone', '60c9bce92ae08'),
('60c9bce933f02', 'Fonction D\'utilité', '60c9bce93564c'),
('60c9bce933f02', 'Utilité', '60c9bce935663'),
('60c9bce933f02', 'Utilité Moyenne', '60c9bce93566b'),
('60c9bce933f02', 'Isoquant', '60c9bce935672'),
('60c9bce93b9f4', 'Canal 2 international ', '60c9bce93ef73'),
('60c9bce93b9f4', 'STV', '60c9bce93ef8b'),
('60c9bce93b9f4', 'Equinoxe', '60c9bce93ef93'),
('60c9bce93b9f4', 'CRTV', '60c9bce93ef9a'),
('60c9bce943d10', 'Courbe D\'indifférence', '60c9bce944d3a'),
('60c9bce943d10', 'Taxes', '60c9bce944de2'),
('60c9bce943d10', 'Epargne', '60c9bce944dec'),
('60c9bce943d10', 'Investissement', '60c9bce944df3'),
('60c9bce949730', 'Oligopole', '60c9bce94a2c6'),
('60c9bce949730', 'Duopole', '60c9bce94a2d7'),
('60c9bce949730', 'Monopole', '60c9bce94a2dd'),
('60c9bce949730', 'Rien de tout ceci', '60c9bce94a2e2'),
('60c9bce94e576', 'Le capital', '60c9bce94f5c4'),
('60c9bce94e576', 'Le travail', '60c9bce94f5d4'),
('60c9bce94e576', 'La Terre', '60c9bce94f5d9'),
('60c9bce94e576', 'Le multiplicateur', '60c9bce94f5de'),
('60c9bce9541e2', 'VAN', '60c9bce955176'),
('60c9bce9541e2', 'TIR', '60c9bce955180'),
('60c9bce9541e2', 'TVA', '60c9bce955186'),
('60c9bce9541e2', 'VA', '60c9bce95518a'),
('60c9bce958df1', 'Livre', '60c9bce959857'),
('60c9bce958df1', 'Internet', '60c9bce959862'),
('60c9bce958df1', 'Téléphone', '60c9bce959867'),
('60c9bce958df1', 'Les soins de santés', '60c9bce95986c'),
('60c9c5ec2f5a7', 'ROM', '60c9c5ec3136d'),
('60c9c5ec2f5a7', 'RAM', '60c9c5ec31386'),
('60c9c5ec2f5a7', 'EPROM', '60c9c5ec3138d'),
('60c9c5ec2f5a7', 'EEPROM', '60c9c5ec31394'),
('60c9c5ec36265', 'Disque', '60c9c5ec36dfe'),
('60c9c5ec36265', 'Clavier', '60c9c5ec36e10'),
('60c9c5ec36265', 'Moniteur', '60c9c5ec36e15'),
('60c9c5ec36265', 'CPU', '60c9c5ec36e1a'),
('60c9c5ec3ab51', '4bits', '60c9c5ec3bae2'),
('60c9c5ec3ab51', '4 KO', '60c9c5ec3baed'),
('60c9c5ec3ab51', '4 Octets', '60c9c5ec3baf2'),
('60c9c5ec3ab51', '4 Mo', '60c9c5ec3baf7'),
('60c9c5ec3faa0', 'RAM', '60c9c5ec40603'),
('60c9c5ec3faa0', 'cache', '60c9c5ec4060d'),
('60c9c5ec3faa0', 'Registre', '60c9c5ec40612'),
('60c9c5ec3faa0', 'Disque Dur', '60c9c5ec40616'),
('60c9c5ec448bd', 'BCD', '60c9c5ec45874'),
('60c9c5ec448bd', 'Décimal', '60c9c5ec45885'),
('60c9c5ec448bd', 'Hexadécimal', '60c9c5ec4588a'),
('60c9c5ec448bd', 'Octal', '60c9c5ec4588f'),
('60c9c5ec4d513', 'Langage D\'assembleur', '60c9c5ec4e113'),
('60c9c5ec4d513', 'Langage au niveau de la machine', '60c9c5ec4e126'),
('60c9c5ec4d513', 'Langage de haut niveau', '60c9c5ec4e12b'),
('60c9c5ec4d513', 'Langage naturel', '60c9c5ec4e130'),
('60c9c5ec51746', 'L\'accumulateur', '60c9c5ec54341'),
('60c9c5ec51746', 'Les registres', '60c9c5ec54353'),
('60c9c5ec51746', 'Le tas', '60c9c5ec54358'),
('60c9c5ec51746', 'La pile', '60c9c5ec5435d'),
('60c9c5ec58b8d', 'Registres', '60c9c5ec5989d'),
('60c9c5ec58b8d', 'Accumulateurs', '60c9c5ec598ae'),
('60c9c5ec58b8d', 'Horloge', '60c9c5ec598b3'),
('60c9c5ec58b8d', 'ensembles de Lignes parallèles', '60c9c5ec598b8'),
('60c9c5ec5db1c', 'Le compilateur', '60c9c5ec5e5eb'),
('60c9c5ec5db1c', 'L\'éditeur', '60c9c5ec5e5fd'),
('60c9c5ec5db1c', 'le navigateur', '60c9c5ec5e602'),
('60c9c5ec5db1c', 'le système D\'exploitation', '60c9c5ec5e607'),
('60c9c5ec61ce3', 'AND', '60c9c5ec62804'),
('60c9c5ec61ce3', 'OR', '60c9c5ec62815'),
('60c9c5ec61ce3', 'NOR', '60c9c5ec6281a'),
('60c9c5ec61ce3', 'toutes les réponses sont vrais', '60c9c5ec6281f'),
('60c9d19935c9a', ' (0, 0, 0) ? E.', '60c9d19a0ee23'),
('60c9d19935c9a', ' E n’est pas stable par addition.', '60c9d19a0ee3c'),
('60c9d19935c9a', 'E est stable par multiplication par un scalaire', '60c9d19a0ee44'),
('60c9d19935c9a', 'E est un espace vectoriel.', '60c9d19a0ee4b'),
('60c9d19a15e20', ' E est non vide.', '60c9d19a16d43'),
('60c9d19a15e20', 'E est stable par addition.', '60c9d19a16d5b'),
('60c9d19a15e20', 'E est un sous-espace vectoriel de R 2 .', '60c9d19a16d63'),
('60c9d19a15e20', ' E est stable par multiplication par un scalaire.', '60c9d19a16d6a'),
('60c9d19a1c708', 'E = {(x, y, z) ? R 3 ; x + y = x + z = 0}.', '60c9d19a1f2e5'),
('60c9d19a1c708', 'E = {(x, y, z) ? R 3 ; x + y = 0} ? {(x, y, z) ? R 3 ; x + z = 0}.', '60c9d19a1f2fc'),
('60c9d19a1c708', 'E = {(x, y, z) ? R 3 ; x + y = 0} ? {(x, y, z) ? R 3 ; x + z = 0}.', '60c9d19a1f304'),
('60c9d19a1c708', 'E n’est pas un espace vectoriel, car E n’est pas stable par addition.', '60c9d19a1f30b'),
('60c9d19a28f4b', '{u1 , u2 , u3 } est une famille libre.', '60c9d19a29e6a'),
('60c9d19a28f4b', '{u1 , u2 , u3 } est une famille génératrice de R 3 .', '60c9d19a29e80'),
('60c9d19a28f4b', 'u3 est une combinaison linéaire de u1 et u2 .', '60c9d19a29e87'),
('60c9d19a28f4b', '{u1 , u2 , u3 } est une base de R 3 .', '60c9d19a29e8e'),
('60c9d19a2e5e2', '{(1,?1, 1)} est une base de ker f .', '60c9d19a2f40c'),
('60c9d19a2e5e2', 'f est injective', '60c9d19a2f41c'),
('60c9d19a2e5e2', '{(1, 0, 1),(0, 1, 1)} est une base de Im f .', '60c9d19a2f423'),
('60c9d19a2e5e2', ' f est surjective.', '60c9d19a2f42a'),
('60c9d814821d9', 'Informe sur l\'existence d\'une variable', '60c9d8148390b'),
('60c9d814821d9', 'Est équivalente à  la fonction defined', '60c9d81483923'),
('60c9d814821d9', 'produit un effet contraire à celui produit par unset', '60c9d8148392b'),
('60c9d814821d9', 'N\'existe pas dans le language php', '60c9d81483932'),
('60c9d8148e3d2', 'L\'adresse IP de la machine serveur', '60c9d8148f53d'),
('60c9d8148e3d2', 'Le nom du client', '60c9d8148f54c'),
('60c9d8148e3d2', 'L\'adresse IP du client', '60c9d8148f551'),
('60c9d8148e3d2', 'Le nom de la machine serveur', '60c9d8148f555'),
('60c9d814957a6', 'Personal Home Page', '60c9d81496d00'),
('60c9d814957a6', 'Php HyperText Preprocessor', '60c9d81496d0d'),
('60c9d814957a6', 'HyperText Preprocessor', '60c9d81496d11'),
('60c9d814957a6', 'Rien de tout ceci', '60c9d81496d16'),
('60c9d8149ceed', '.html', '60c9d8149df6d'),
('60c9d8149ceed', '.xml', '60c9d8149df7b'),
('60c9d8149ceed', '.php', '60c9d8149df7f'),
('60c9d8149ceed', '.ph', '60c9d8149df83'),
('60c9d814a3530', 'L\'accès au document n\'est pas autorisé', '60c9d814a6228'),
('60c9d814a3530', 'il y a une erreur de syntaxe dans l\'adresse du document', '60c9d814a623b'),
('60c9d814a3530', 'l\'accès au serveur n\'est pas autoris', '60c9d814a623f'),
('60c9d814a3530', 'le fichier php n\'existe pas', '60c9d814a6244'),
('60c9d814abb74', 'Langage de programmation ', '60c9d814ac9be'),
('60c9d814abb74', 'langage POO', '60c9d814ac9cf'),
('60c9d814abb74', 'Langage de haut niveau', '60c9d814ac9d4'),
('60c9d814abb74', 'Langage de Balisage', '60c9d814ac9d8'),
('60c9d814afd7b', 'Design', '60c9d814b082f'),
('60c9d814afd7b', 'Style ', '60c9d814b0842'),
('60c9d814afd7b', 'Modify', '60c9d814b0847'),
('60c9d814afd7b', 'Define', '60c9d814b084b'),
('60c9d814b4141', 'tue un processus', '60c9d814b4c6c'),
('60c9d814b4141', 'arrete le script', '60c9d814b4c7b'),
('60c9d814b4141', 'Sort d\'une boucle', '60c9d814b4c7f'),
('60c9d814b4141', 'bypasse une erreur', '60c9d814b4c84'),
('60c9d814b8a63', 'sum()', '60c9d814b95e9'),
('60c9d814b8a63', 'call function sum()', '60c9d814b95f2'),
('60c9d814b8a63', 'call sum()', '60c9d814b95f7'),
('60c9d814b8a63', 'rien de tout ceci', '60c9d814b95fb'),
('60c9d814bc6ba', 'function f()', '60c9d814bd1d6'),
('60c9d814bc6ba', 'function = f()', '60c9d814bd1e6'),
('60c9d814bc6ba', 'function:f()', '60c9d814bd1eb'),
('60c9d814bc6ba', 'rien de tout ceci', '60c9d814bd1ef');

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE `questions` (
  `code` varchar(20) NOT NULL,
  `idquestion` text NOT NULL,
  `question` text NOT NULL,
  `nbre_proposition` int(10) NOT NULL,
  `num_question` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `questions`
--

INSERT INTO `questions` (`code`, `idquestion`, `question`, `nbre_proposition`, `num_question`) VALUES
 
('INF222', '60c9b05071639', 'Lequel n\'est pas un modèle de développement d\'un projet ? ', 4, 1),
('INF222', '60c9b0508376a', 'Quel est l\'intrus parmis c\'est tache du génie logiciel ?', 4, 2),
('INF222', '60c9b0508aad9', 'Leque de ces diagrammes n\'est ni structurel ni statique ?', 4, 3),
('INF222', '60c9b05095eff', 'Que signie l\'acronyme UML ?', 4, 4),
('INF222', '60c9b0509bf9c', 'Lequel ne fait pas partie des qualités d\'un bon logiciel ? ', 4, 5),
('INF221', '60c9b6adca281', 'Un SI est l\'abréviation de :', 4, 1),
('INF221', '60c9b6add4b26', 'Quelles sont les fonctions prinipales d\'un SI :', 4, 2),
('INF221', '60c9b6addbe69', 'Quel Diagramme permet de décrire comment les données sont structurées ?', 4, 3),
('INF221', '60c9b6aded611', 'Dans un Diagramme de flux , Quels sont les Types d\'acteurs que l\'on peut représenter ?', 4, 4),
('INF221', '60c9b6ae00b97', 'Dans un diagramme de flux ,un flux relie quoi à quoi ?', 4, 5),
('INF262', '60c9bce929127', 'Choisir l\'élément étranger ', 4, 1),
('INF262', '60c9bce933f02', 'Choisir l\'élément étranger ', 4, 2),
('INF262', '60c9bce93b9f4', 'Choisir l\'élément étranger ', 4, 3),
('INF262', '60c9bce943d10', 'Choisir l\'élément étranger ', 4, 4),
('INF262', '60c9bce949730', 'Choisir l\'élément étranger ', 4, 5),
('INF262', '60c9bce94e576', 'Choisir l\'élément étranger ', 4, 6),
('INF262', '60c9bce9541e2', 'Choisir l\'élément étranger ', 4, 7),
('INF262', '60c9bce958df1', 'Choisir l\'élément étranger ', 4, 8),
('INF252', '60c9c5ec2f5a7', 'La mémoire qui permet l,opération de lecture et d\'écriture simultanées est ?', 4, 1),
('INF252', '60c9c5ec36265', 'Qui n\'est pas considéré comme un périphérique de l\'ordinateur ?', 4, 2),
('INF252', '60c9c5ec3ab51', 'La taille du resgistre accumulateur peut etre de ?', 4, 3),
('INF252', '60c9c5ec3faa0', 'Le composant informatique le plus rapide est ?', 4, 4),
('INF252', '60c9c5ec448bd', 'Le format ______ est genéralement utilisé pour stocker des données.', 4, 5),
('INF252', '60c9c5ec4d513', 'Un programme source est généralement écrit en ?', 4, 6),
('INF252', '60c9c5ec51746', 'L\'ALU utilise pour stocker les résultats intermédiaires ?', 4, 7),
('INF252', '60c9c5ec58b8d', 'Un bus informatique est composé de ?', 4, 8),
('INF252', '60c9c5ec5db1c', 'L\'ordinateur ne peut pas démarrer s\'il n\'a pas ?', 4, 9),
('INF252', '60c9c5ec61ce3', 'Exemple D\'operateur Logique:', 4, 10),
('INF242', '60c9d19935c9a', 'Soit E = {(x, y, z) ∈ R\r\n3\r\n; x − y + z = 0}, muni des opérations usuelles. Quel es\r\nl\'assertions vraie ?', 4, 1),
('INF242', '60c9d19a15e20', 'Soit E = {(x, y) ∈ R\r\n2\r\n; x − y ¾ 0}, muni des opérations usuelles. Quel est l\'assertions vraie ?', 4, 2),
('INF242', '60c9d19a1c708', 'Soit E = {(x, y, z) ∈ R\r\n3\r\n; (x + y)(x +z) = 0}, muni des opérations usuelles. Quel  est l\'assetion vraie ?', 4, 3),
('INF242', '60c9d19a28f4b', 'Dans R\r\n3\r\n, on considère les vecteurs u1 = (1, 1, 0), u2 = (0, 1,−1) et u3 = (−1, 0,−1). Quel est l\'assertion vraie', 4, 4),
('INF242', '60c9d19a2e5e2', 'On considère l’ application linéaire :\r\nf : R\r\n3 → R\r\n3\r\n(x, y, z) → (x − z, y + z, x + y) quel est la bonne reponse ?', 4, 5),
('INF232', '60c9d814821d9', 'La fonction Isset :', 4, 1),
('INF232', '60c9d8148e3d2', 'Que contient la variable <<$_SERVER[\'REMOTE_ADDR\']>>', 4, 2),
('INF232', '60c9d814957a6', 'Que signifie l\'acronyme PHP ?', 4, 3),
('INF232', '60c9d8149ceed', 'Les fichiers PHP ont l\'extension... ? ', 4, 4),
('INF232', '60c9d814a3530', 'Que deduire si l\'on obtient l\'erreur 404 lors de l\'accès à un fichier php ?', 4, 5),
('INF232', '60c9d814abb74', 'HTML est considéré comme ?', 4, 6),
('INF232', '60c9d814afd7b', 'La balise HTML qui spécifie un style CSS intégré dans un élément est appelée ?', 4, 7),
('INF232', '60c9d814b4141', 'Que fait die() ?', 4, 8),
('INF232', '60c9d814b8a63', 'Comment faire appelle à une fonction nommée sum ?', 4, 9),
('INF232', '60c9d814bc6ba', 'Comment créer une fonction en JavaScript ?', 4, 10);

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `idquestion` text NOT NULL,
  `idreponse` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reponse`
--

INSERT INTO `reponse` (`idquestion`, `idreponse`) VALUES
('60bb597963147', '60bb59798d702'),
('60bbfb76b09ff', '60bbfb76cde5d'),
('60bdd1f8a4240', '60bdd1f8bab61'),
('60bdd1f92c8ed', '60bdd1f93cac2'),
('60bdd1f9a1f5d', '60bdd1f9b00a5'),
('60bdd1fa080d9', '60bdd1fa15f5d'),
('60bdd1fa72451', '60bdd1fa7ea68'),
('60c901e8e48cc', '60c901e8e774f'),
('60c901e8eec2e', '60c901e8f011f'),
('60c901e9080c4', '60c901e90bc8e'),
('60c901e9121d8', '60c901e91407b'),
('60c901e91a28d', '60c901e91b818'),
('60c901e921b9a', '60c901e922ef5'),
('60c901e929105', '60c901e92a634'),
('60c901e930765', '60c901e931ca2'),
('60c901e93693d', '60c901e937717'),
('60c901e93c2b8', '60c901e93d1ad'),
('60c9b05071639', '60c9b05079935'),
('60c9b0508376a', '60c9b05084a00'),
('60c9b0508aad9', '60c9b0508c3e8'),
('60c9b05095eff', '60c9b05096f0c'),
('60c9b0509bf9c', '60c9b0509ece2'),
('60c9b6adca281', '60c9b6adcc28a'),
('60c9b6add4b26', '60c9b6add5fec'),
('60c9b6addbe69', '60c9b6addd20f'),
('60c9b6aded611', '60c9b6adf007e'),
('60c9b6ae00b97', '60c9b6ae01b4f'),
('60c9bce929127', '60c9bce92ae08'),
('60c9bce933f02', '60c9bce935672'),
('60c9bce93b9f4', '60c9bce93ef9a'),
('60c9bce943d10', '60c9bce944d3a'),
('60c9bce949730', '60c9bce94a2e2'),
('60c9bce94e576', '60c9bce94f5de'),
('60c9bce9541e2', '60c9bce955186'),
('60c9bce958df1', '60c9bce95986c'),
('60c9c5ec2f5a7', '60c9c5ec31386'),
('60c9c5ec36265', '60c9c5ec36e1a'),
('60c9c5ec3ab51', '60c9c5ec3baf2'),
('60c9c5ec3faa0', '60c9c5ec40612'),
('60c9c5ec448bd', '60c9c5ec4588a'),
('60c9c5ec4d513', '60c9c5ec4e12b'),
('60c9c5ec51746', '60c9c5ec54341'),
('60c9c5ec58b8d', '60c9c5ec598b8'),
('60c9c5ec5db1c', '60c9c5ec5e607'),
('60c9c5ec61ce3', '60c9c5ec6281f'),
('60c9d19935c9a', '60c9d19a0ee4b'),
('60c9d19a15e20', '60c9d19a16d63'),
('60c9d19a1c708', '60c9d19a1f30b'),
('60c9d19a28f4b', '60c9d19a29e6a'),
('60c9d19a2e5e2', '60c9d19a2f40c'),
('60c9d814821d9', '60c9d8148390b'),
('60c9d8148e3d2', '60c9d8148f551'),
('60c9d814957a6', '60c9d81496d0d'),
('60c9d8149ceed', '60c9d8149df7f'),
('60c9d814a3530', '60c9d814a6244'),
('60c9d814abb74', '60c9d814ac9d8'),
('60c9d814afd7b', '60c9d814b0842'),
('60c9d814b4141', '60c9d814b4c7b'),
('60c9d814b8a63', '60c9d814b95e9'),
('60c9d814bc6ba', '60c9d814bd1d6');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`ide`),
  ADD UNIQUE KEY `matricule` (`matricule`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`SN`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `ide` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `SN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
