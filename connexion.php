<?php
    include_once'dbconnection.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Project Web || COMPOZ ON LINE </title>
        <link  rel="stylesheet" href="sstyle.css"/>  
        <link  rel="stylesheet" href="bootstrap.min.css"/>
        <link  rel="stylesheet" href="bootstrap-theme.min.css"/>   
        <link rel="stylesheet" href="main.css">
        <script src="jquery.js" type="text/javascript"></script>
        <script src="bootstrap.min.js"  type="text/javascript"></script>
            
    </head>
<body>

<header>
        <div id="wrapper">
            <nav>
                <div id="menu1">
                    <a href="#" class="col"><span>COL</span>Compoz On line</a>
                    <a href="inscription.php" class="con" >s'inscrire</p></a>
                    <a href="accueil.php" class="con">Accueil</a>
                    <a href="#" class="con  sub1" data-toggle="modal" data-target="#myModal"><span  aria-hidden="true"></span>&nbsp;<span class="title1"><b>Je suis Admin</b></span></a></div>
            
                </div>
                <img class="Accueil" src="bg.jpg" alt="Accueil">
            </nav>
        </div>
    </header>
    <div class="wrapper2">
        <section>
        <form action="login.php" method="POST">
           <div class="connect">
                <h1>Connexion</h1>
                <input type="text" name="email" class="box" placeholder="Entrer votre Email"><br>
                <input type="password" name="password" class="box" required="required" placeholder="Entrer votre password"> <br><br>
                <input type="submit" name="login" id="login" value="Login"><br><br>
            </div>
           </form>
        </section>
        
    </div>
    <div class="col-md-2 col-md-offset-4">
            <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content title1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="btn-danger">&times;</span></button>
                    <h4 class="modal-title title1">ESPACE ADMIN : Connexion</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="adminconnect.php" method="POST">
            <fieldset>


            <!-- Text input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="email"></label>  
            <div class="col-md-6">
            <input id="email" name="email" placeholder="Enter votre id-email" class="form-control input-md" type="email">
                
            </div>
            </div>


            <!-- Password input-->
            <div class="form-group">
            <label class="col-md-3 control-label" for="password"></label>
            <div class="col-md-6">
                <input id="password" name="password" placeholder="Enter votre Password" class="form-control input-md" type="password">
                
            </div>
            </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">fermer</button>
                    <button type="submit" class="btn btn-success" name="connect">Se connecter</button>
                    </fieldset>
            </form>
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--sign in modal closed-->

            </div><!--header row closed-->
            </div>
    
    
</body>
</html>