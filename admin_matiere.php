<!DOCTYPE html>
<?php include_once 'dbConnection.php';
session_start();
$email=$_SESSION['email'];
  if(!(isset($_SESSION['email']))){
header("location:accueil.php");

}
else
{
$name = $_SESSION['name'];

include_once 'dbConnection.php';

if(isset($_POST['suivant']))
 {
   $nom = htmlspecialchars($_POST['nom']);
   $code = htmlspecialchars($_POST['code']);
   $total = htmlentities($_POST['total']);
   $juste = htmlentities($_POST['juste']);
   $fausse = htmlentities($_POST['fausse']);
   $reqcode= $bdd->prepare("SELECT * FROM matiere WHERE code=?");
   $reqcode->execute(array($code));
   $codeexist=$reqcode->rowCount();
   if ($codeexist==0) {
    $reqnom = $bdd->prepare("SELECT * FROM matiere WHERE nom=?");
    $reqnom->execute(array($nom));
    $nomexist=$reqnom->rowCount();
   if ($nomexist==0) {
      if ($total>0) {
     if ($juste>0) {
        if ($fausse<=0) {
          $query= "INSERT INTO matiere(code,nom,total_question,juste,fausse) VALUES (?,?,?,?,?)";
          $inscrit=$bdd->prepare($query);
          $inscrit->execute(array($code, $nom, $total, $juste, $fausse));
          header("location:quiz.php?q=4&step=2&eid=$code&n=$total");

        }else{
          $erreur="le nombre de point doit etre negatif";
        }
   }else{
    $erreur="le nombre de point doit etre positif";
   }
      }else{
        $erreur="le nombre de questions doit etre superieur a zero";
      }
    }else{
      $erreur="le nom existe deja";
    }
   }else{

    $erreur="le code existe deja";
   }
   
 }
}

?>
  <html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Projet web || COMPOZ_ON_LINE</title>
    <link rel="stylesheet" type="text/css" href="sstyle.css">
    
 <link  rel="stylesheet" href="css/bootstrap-theme.min.css"/>    
 <script src="js/jquery.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js"  type="text/javascript"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
<header>
        <div id="wrapper">
            <nav>
                <div id="menu1">
                    <a href="#" class="col"><span>COL</span>Compoz On line</a>
                    <a href="logout.php" class="con">| déconnexion</a>
                    <?php if(isset($name)){ echo '<a href="adminHome.php" class="con"><b>Hello, </b>' .$name. '</a>';} ?>
                </div>
               
            </nav>
        </div>
    </header>
    <section>
       
   
   <nav >
        <ul id="menu">
          <li><a href="#" class="active">Menu</a></li>
          <li><a href="historik.php">Historique</a></li>
          <li><a href="classik.php">Classification</a></li>
          <li><a href="lire_requete.php">Requete</a></li>>
          <li><a href="#">Matiere</a>
          <ul>
            
          <li <?php if(@$_GET['q']==4) echo'class="active"'; ?>><a href="admin_matiere.php?q=4">ajouter</a></li>
            <li <?php if(@$_GET['q']==5) echo'class="active"'; ?>><a href="admin_matiere.php?q=5">supprimer</a></li>
          </ul>
          </li>
          <li> <a href="listEtudiant.php">Etudiant</a></li> 
          <li><a href="logout.php">Déconnexion</a></li>


        </ul>
      </nav>

      <div class="content1">
      <?php
      if(@$_GET['q']==4) {
        ?>
      <div id="quiz">
    <form class="" action="" method="post">
      <label for="nom">Code de la matiere: </label><br>
      <input type="text" name="code" class="box" placeholder="Entrer le code de la matiere" required value="<?php if (isset($code)) { echo $code;}?>"><br>
      <label for="nom">Nom de la matiere: </label><br>
      <input type="text" name="nom" class="box" placeholder="Entrer le nom de la matiere" required value="<?php if (isset($nom)) { echo $nom;}?>"><br/>
      <label for="total">Total Questions: </label><br>
      <input type="text" name="total" class="box" placeholder="Entrer le total de questions" required value="<?php if (isset($total)) { echo $total;}?>"><br/>
      <label for="email">Point reponse juste: </label><br>
      <input type="text" name="juste" class="box" placeholder="Entrer le point" required value="<?php if (isset($juste)) { echo $juste;}?>"><br/>
      <label for="email">Point reponse fausse: </label><br>
      <input type="text" name="fausse"  class="box" placeholder="Entrer le point" required value="<?php if (isset($fausse)) { echo $fausse;}?>"><br/>
      <br><br>
      
  
  <?php
      if (isset($erreur)) {
        echo $erreur;
      }
      ?>
    <input type="submit" name="suivant" value="Suivant">
     
    </form>
    <?php
      }
    ?> 
    </div>
    </div>
    <?php 
if(@$_GET['q']==5) {
  if(@$_GET['message'])
    {echo'<script>alert("'.@$_GET['message'].'");</script>';}
  $result =$bdd->query("SELECT * FROM matiere ORDER BY code DESC") ;
  echo  '<div class="panel"><div class="table-responsive"><table class="table table-striped title1">
  <caption>SUPPRIMER UNE MATIERE</caption>
  <tr><td><b>num</b></td><td><b>matiere</b></td><td><b>Total question</b></td><td><b>noté sur</b></td><td></td></tr>';
  $c=1;
  while($row = $result->fetch()) {
    $title = $row['nom'];
    $total = $row['total_question'];
    $sahi = $row['juste'];
      
    $eid = $row['code'];
    echo '<tr><td>'.$c++.'</td><td>'.$title.'</td><td>'.$total.'</td><td>'.$sahi*$total.'</td>
    <td><b><a href="quiz.php?q=rmquiz&eid='.$eid.'" class="btn-dander btn-lg " id="inscrire" style="margin:0px;padding:10px;background:red"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;<span class="title1"><b>supprimer</b></span></a></b></td></tr>';
  }
  $c=0;
  echo '</table></div></div>';
  
  }
  ?>
      
    </section>
    
</body>
</html>